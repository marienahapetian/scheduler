<?php

use Faker\Generator as Faker;

$factory->define(App\Article::class, function (Faker $faker) {
    return [
        'title' => $faker->text(30),
        'description' => $faker->text,
        'content' => $faker->text,
        'thumbnail' => $faker->imageUrl(),
        'user_id' => 1
    ];
});
