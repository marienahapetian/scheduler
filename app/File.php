<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'videos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'filename', 'thumbnail', 'user_id','title'
    ];

    public function user()
    {
        return $this->belongsTo('\App\User');
    }
}
