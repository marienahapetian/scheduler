<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'thumbnail','content','author','user_id','source','url','created_at'
    ];


    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
