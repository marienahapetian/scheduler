<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FileController extends Controller
{
    public function index()
    {
        $videos = File::all();
        return view('videos',compact('videos'));
    }

    public function __construct()
    {

    }

    public function upload()
    {
        return view('fileupload');
    }

    public function store(Request $request)
    {
        $file = $request->file;
        $filename = $file->getClientOriginalName();
        $ext = $file->getClientOriginalExtension();
        $filename = str_replace('.' . $ext, '', $filename);

        if (!is_dir('uploads/videos')) {
            mkdir('uploads/videos');
        }

        $file->move('uploads/videos/', $file->getClientOriginalName());

        if (!is_dir('uploads/images')) {
            mkdir('uploads/images');
        }

        $img = $request->thumbnail;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);

        $thumb_uri = 'uploads/images/' . $filename . '.png';

        file_put_contents($thumb_uri, $data);

        File::create([
            'filename' => $file->getClientOriginalName(),
            'thumbnail' => $thumb_uri,
            'user_id' => Auth::user()->id,
            'title' => $request->title?:''
        ]);

        return redirect('/videos')->with('status','Video uploaded successfully');

    }
}
