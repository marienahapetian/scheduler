<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mtv = \App\Article::where('source','MTV News')->get();
        $bleacher = \App\Article::where('source','Bleacher Report')->get();
        $news = \App\Article::where('source','News.am')->get();

        $totalArticles = count($mtv) + count($bleacher) + count($news);

        $dataPoints = array(
            array("y"=> count($mtv), "label"=> "MTV News", "color"=> "#fff400"),
            array("y"=> count($bleacher), "label"=> "Bleacher Report", "color"=> "#000"),
            array("y"=> count($news), "label"=> "News.am", "color"=> "#e90606"),
        );

        return view('home',compact('mtv','news','bleacher','dataPoints','totalArticles'));
    }
}
