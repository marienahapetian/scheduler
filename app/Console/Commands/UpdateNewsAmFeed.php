<?php

namespace App\Console\Commands;

use App\Article;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateNewsAmFeed extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsamfeed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get latest articles from bleach reports and update local news feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', "https://news.am/eng/");

        $statusCode = $response->getStatusCode();
        $html = $response->getBody();

        if ($statusCode == '200') {
            $dom = new \DOMDocument();
            libxml_use_internal_errors(true);
            $dom->loadHTML($html);
            libxml_use_internal_errors(false);

            $news1 = getElementsByClassName($dom, 'news-item', 'a');
            $news11 = getElementsByClassName($dom, 'news-item ', 'a');
            $news2 = getElementsByClassName($dom, 'news-item border', 'a');
            $news3 = getElementsByClassName($dom, 'news-item big', 'a');

            $news_array = array();

            foreach ($news1 as $single_news) {
                $news_array[] = $single_news->getAttribute('href');
            }
            foreach ($news11 as $single_news) {
                $news_array[] = $single_news->getAttribute('href');
            }

            foreach ($news2 as $single_news) {
                $news_array[] = $single_news->getAttribute('href');
            }

            foreach ($news3 as $single_news) {
                $news_array[]=$single_news->getAttribute('href');
            }

            foreach (array_unique($news_array) as $key => $single_news_url){
                try {
                    $this->updateSingleArticle($single_news_url,$key);
                } catch (\Exception $e) {
                    //dd($single_news_url);
                    dd($e);
                }

            }
        }

        return redirect()->back();
    }

    public function updateSingleArticle( $url,$key )
    {
        $client = new \GuzzleHttp\Client();

        // check page type
        if (substr($url, 0, 7) == '//style' || substr($url, 0, 13) == 'https://style') {
            $type = 'style';
        } else if (substr($url, 0, 5) == '//med' || substr($url, 0, 11) == 'https://med') {
            $type = 'med';
        } else if (substr($url, 0, 7) == '//sport' || substr($url, 0, 13) == 'https://sport') {
            $type = 'sport';
        } else if (substr($url, 0, 11) == '/eng/videos' || substr($url, 0, 26) == 'https://news.am/eng/videos') {
            $type = 'videos';
        } else if (substr($url, 0, 3) == 'eng' || substr($url, 0, 4) == '/eng' || substr($url, 0, 11) == 'https://news') {
            $type = 'news';
        }

        // define base_url
        if (substr($url, 0, 3) == 'eng') {
            $base_url = 'https://news.am/';
        } elseif (substr($url, 0, 4) == '/eng') {
            $base_url = 'https://news.am';
        } else {
            $base_url = '';
        }


        // select article with given url
        $url_exists = count(Article::where('url','LIKE', '%'.$url)->get());

        // if article does not exist, create it
        if (!$url_exists) {
            $response = $client->request('GET', $base_url . $url);

            $statusCode = $response->getStatusCode();
            $html = $response->getBody();

            if ($statusCode == '200') {
                $dom = new \DOMDocument();
                libxml_use_internal_errors(true);
                $dom->loadHTML($html);
                libxml_use_internal_errors(false);

                if(in_array($type,['med','sport',])){
                    $title_node = $dom->getElementsByTagName('h1');
                    $title = $title_node[0]->nodeValue;
                } else {
                    $title_node = $dom->getElementsByTagName('title');
                    $title = $title_node[0]->nodeValue;
                }

                $title_exists = count(Article::where('title', $title)->get());

                if (!$title_exists && $title != 'Armenia News - NEWS.am') {
                    switch ($type) {
                        case 'sport':
                            $content_node = $dom->getElementById('opennewstext');
                            $date_node = getElementsByClassName($dom, 'date', 'span')[0];
                            $time_node = getElementsByClassName($dom, 'time', 'span')[0];

                            $date = str_replace('+04,', '', str_replace("\xc2\xa0", '', $date_node->nodeValue));
                            $date = $date . ' ' . $time_node->nodeValue;
                            $date = str_replace("\xc2\xa0", '', $date);
                            $created_at = Carbon::parse($date);

                            break;
                        case 'med':
                            $content_node = $dom->getElementById('opennewstext');
                            $date_node = getElementsByClassName($dom, 'time', 'div')[0];
                            $date = str_replace("\xc2\xa0", '', $date_node->nodeValue);
                            $created_at = Carbon::parse($date);

                            break;
                        case 'style':
                            $content_node = $dom->getElementById('opennewstext');
                            $date_node = getElementsByClassName($dom, 'date', 'span')[0];
                            $date = str_replace("\xc2\xa0", '', $date_node->nodeValue);
                            $created_at = Carbon::parse($date);
                            break;
                        case 'videos':
                            $content_node = getElementsByClassName($dom, 'video-player', 'div')[0];
                            $date_node = getElementsByClassName($dom, 'time', 'div')[0];
                            $date = str_replace("\xc2\xa0", '', $date_node->nodeValue);
                            $created_at = Carbon::parse($date);

                            break;
                        case 'news':
                            $content_node = getElementsByClassName($dom, 'article-body', 'span')[0];
                            $date_node = getElementsByClassName($dom, 'time', 'div')[0];
                            $date = str_replace("\xc2\xa0", '', $date_node->nodeValue);
                            $created_at = Carbon::parse($date);

                            break;
                    }



                    $content = '';
                    $thumbnail = 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/300px-No_image_available.svg.png';

                    // iframes in the page
                    $iframes = $dom->getElementsByTagName('iframe');
                    foreach ($iframes as $iframe) {
                        if ($iframe->parentNode->getAttribute('class') == 'article-text' || $iframe->parentNode->getAttribute('class') == 'video-player')
                            $content .= '<div class="iframe-wrapper"><iframe src="' . $iframe->getAttribute('src') . '" width="' . $iframe->getAttribute('width') . '" height="' . $iframe->getAttribute('height') . '"></iframe></div>' . ' <br>';
                    }

                    // images in the page
                    $images = $dom->getElementsByTagName('img');
                    foreach ($images as $image) {
                        if ($image->parentNode->getAttribute('class') == 'article-text' || $image->parentNode->getAttribute('id') == 'opennewstext' ) {
                            if (substr($image->getAttribute('src'), 0, 7) == '/static') {
                                $base = explode('/', $url);
                                $thumbnail = $base[0] . '//' . $base[2] . '/' . $image->getAttribute('src');
                            } else {
                                $thumbnail = $base_url . '/' . $image->getAttribute('src');
                            }
                        }

                        if($image->getAttribute('class')=='myphotos'){
                            if (substr($image->getAttribute('src'), 0, 7) == '/static') {
                                $base = explode('/', $url);
                                $src = $base[0] . '//' . $base[2] . '/' . $image->getAttribute('src');
                            } else {
                                $src = $base_url . '/' . $image->getAttribute('src');
                            }

                            $content .= '<img src="'.$src.'"/>';
                        }
                    }

                    // page text content
                    $children = $content_node->childNodes;
                    foreach ($children as $child) {
                        if ($child->nodeName == 'p') $content .= '<p>' . $child->nodeValue . ' </p>';
                    }

                    Article::create([
                        'title' => $title,
                        'content' => $content,
                        'description' => '',
                        'source' => 'News.am',
                        'url' => $base_url . $url,
                        'thumbnail' => $thumbnail,
                        'user_id' => 1,
                        'created_at' => $created_at
                    ]);
                }
            } //end url exists case
        } //end article does not exist case
    }

}