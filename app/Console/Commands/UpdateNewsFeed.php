<?php

namespace App\Console\Commands;

use App\Article;
use Carbon\Carbon;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class UpdateNewsFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsfeed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get latest articles from bleach reports and update local news feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $http = new Client;

        $response = $http->get('https://newsapi.org/v2/top-headlines?sources=bleacher-report,mtv-news&apiKey=8df92bd7d7e2456bb1f3800c2ed131e7');

        $result = json_decode((string)$response->getBody(), true);

        $articles = $result['articles'];

        foreach ($articles as $article) {
            /* if article with this title does not exist then add */
            if (!count(Article::where('url', $article['url'])->get())) {

                Article::create([
                    'title' => $article['title'],
                    'description' => $article['description'],
                    'content' => $article['content'],
                    'thumbnail' => $article['urlToImage'],
                    'created_at' => Carbon::parse(str_replace(array('T', 'Z'), ' ', $article['publishedAt'])),
                    'author' => $article['author'],
                    'source' => $article['source']['name'],
                    'url' => $article['url'],
                    'user_id' => 1
                ]);
            }
        }

        return redirect()->back();
    }
}
