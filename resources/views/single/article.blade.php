@extends('templates.main')

@section('content')
    <div class="col-md-8 offset-md-2">
        <h3 class="row page-title">{{ $article->title }}</h3>

        <div class="info row">
            <div class="col-md-12">{{ \Carbon\Carbon::parse($article->created_at)->format('F d,Y H:i:s') }}</div>
            <div class="col-md-12">{{ __('Author: ').$article->user->name }}</div>
            <div class="col-md-12">{!! __('Source: '). '<a href="'.$article->url.'">'.$article->source.'</a>' !!}</div>
        </div>

        <div>
            <img src="{{$article->thumbnail}}" class="float-md-left" style="margin: 0 15px 15px 0"/>
            <div class="article-content">
                {!! $article->content !!}
            </div>
        </div>
    </div>
@endsection