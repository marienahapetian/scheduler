@extends('templates.main')

@section('content')
    <div class="container" style="margin-top: 30px;">
        <div class="page-title" style="margin-bottom: 20px; display: flex; justify-content: space-between;">
            <h2 style="margin-bottom: 0">{{__('Videos')}}</h2>

            @if(\Illuminate\Support\Facades\Auth::check())
                <a class="btn btn-dark" href="/video/upload">
                    {{__('Upload New')}}
                </a>
            @endif
        </div>

        <div class="row">
            @foreach($videos as $video)
                <div class="col-sm-4 video">
                    <img src="{{$video->thumbnail}}"/>
                    <p>
                        <span>{{$video->title}}</span>
                        <span>{{$video->user->name}}</span>
                    </p>
                    <div class="overlay">
                        <i class="far fa-play-circle"></i>
                    </div>
                    <video src="uploads/videos/{{$video->filename}}" controls></video>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/file.js')}}" type="text/javascript" defer></script>
@endsection
