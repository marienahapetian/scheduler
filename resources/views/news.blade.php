@extends('templates.main')

@section('content')
    <div class="col-md-8 offset-md-2">
        <h2 class="row page-title">{{__('Recent News')}}</h2>

        @include('parts.searchform')

        @if(count($articles))
            @foreach($articles as $single_news)
                @include('loop.news')
            @endforeach

            {{ $articles->links() }}
        @else
            {{__('No News Yet.. Hang on there')}}
        @endif

    </div>
@endsection