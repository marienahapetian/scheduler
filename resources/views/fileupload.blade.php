@extends('templates.main')

@section('content')
    <div class="container page-content" style="margin-top: 30px">
        <h2 class="page-title">{{__('Upload Video Files with Screenshots')}}</h2>
        <form method="post" action="/video/store" enctype="multipart/form-data">
            @csrf
            <p><input type="text" name="title" placeholder="Video Title"></p>

            <div class="upload-btn-wrapper">
                <button class="btn">{{__('Select Video')}}</button>
                <input type="file" name="file" id="file" accept="video/mp4,video/x-m4v,video/*"/>
            </div>
            <p class="info">{{__('After selecting the video, play and pause it on preferred moment for screenshot')}}</p>
            <input type="hidden" name="thumbnail" id="thumb-url-holder">
            <input type="submit" value="Upload">
        </form>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-6" id="video-block">

            </div>
            <div class="col-sm-6" id="screenshot-block">
                <img id="thumbnail"/>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" defer>
        document.addEventListener("DOMContentLoaded", function(event) {
            var input = document.getElementById('file');
            var img = document.getElementById('thumbnail');
            var thumbnail = document.getElementById('thumb-url-holder');
            var video_block = document.getElementById('video-block');

            input.addEventListener('change', function (event) {
                var file = this.files[0];
                var url = URL.createObjectURL(file);

                var video = document.createElement('video');
                video.src = url;
                video.controls = true;

                var snapshot = function () {
                    var duration = video.duration;
                    var videoWidth = video.videoWidth;
                    var videoHeight = video.videoHeight;

                    var canvas = document.createElement('canvas');
                    canvas.width  = videoWidth;
                    canvas.height = videoHeight;
                    var ctx = canvas.getContext('2d');

                    ctx.drawImage( video, 0, 0);
                    img.src = canvas.toDataURL('image/png');
                    thumbnail.value = img.src;

                    video.removeEventListener('canplay', snapshot);
                };

                video.addEventListener('pause', snapshot);

                video_block.appendChild(video);
            });
        });
    </script>
@endsection