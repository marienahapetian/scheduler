@extends('templates.main')

@section('content')
    <div class="col-md-8 offset-md-2">
        <h2 class="row page-title">{{__('Users')}}</h2>

        @foreach($users as $user)
            @include('loop.user')
        @endforeach

        {{ $users->links() }}
    </div>
@endsection