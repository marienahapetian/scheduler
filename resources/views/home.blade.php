@extends('templates.main')

@section('header-scripts')
    <script>
        window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                exportEnabled: true,
                title:{
                    text: "Articles by sources"
                },
                data: [{
                    type: "pie",
                    showInLegend: "true",
                    legendText: "{label}",
                    indexLabelFontSize: 16,
                    indexLabel: "{label} - #percent%",
                    yValueFormatString: "#,##0",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart.render();

        }
    </script>
@endsection

@section('content')
    <div id="chartContainer" style="height: 370px; width: 100%;"></div>
@endsection

@section('scripts')
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
@endsection
