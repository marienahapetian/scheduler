<div class="row single-news">
    <div class="col-md-4 no-padd">
        <img src="{{ $single_news->thumbnail }}"/>
    </div>
    <div class="col-md-8">
        <h3>{{ $single_news->title }}</h3>
        <span class="grey-info {{str_replace(array(' ','.'),'_',strtolower($single_news->source))}}">
            {{ $single_news->source?:config('app.name', 'Laravel') }}
        </span>
        <div>
            {!! $single_news->description?:str_limit(preg_replace("/<img[^>]+\>/i", "", $single_news->content),300) !!}
        </div>
        <div class="row info">
            <div class="col-md-6">
                {{ __('Author: ') }}
                {{ $single_news->author ?: $single_news->user->name }}
            </div>
            <div class="col-md-6 text-right">
                {{  \Carbon\Carbon::parse($single_news->created_at)->format('H:i, F d ') }}
            </div>
            <p class="col-md-12"> <a href="{{route('article',$single_news->id)}}">{{__('Read More')}}</a> </p>

        </div>
    </div>
</div>