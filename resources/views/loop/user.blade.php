<div class="row single-news">
    <div class="col-md-3">
        <img src="{{ $user->thumbnail }}"/>
    </div>
    <div class="col-md-9">
        <h3>{{ $user->name }}</h3>
        <p>{{ $user->email }}</p>
        <div class="row info">

        </div>
    </div>
</div>