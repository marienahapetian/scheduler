<form method="get" action="">
    <input type="text" name="search" placeholder="Search by title" value="{{$search}}">
    <div style="margin-top: 10px">
        <label class="checkbox-container">
            <input type="checkbox" name="source[]" value="Bleacher Report" {{!empty($sourceA) && in_array('Bleacher Report',$sourceA)?'checked':''}}>{{__('Bleacher Report')}}
            <span class="checkmark"></span>
        </label>
        <label class="checkbox-container">
            <input type="checkbox" name="source[]" value="MTV News"  {{!empty($sourceA) && in_array('MTV News',$sourceA)?'checked':''}}>{{__('MTV News')}}
            <span class="checkmark"></span>
        </label>
        <label class="checkbox-container">
            <input type="checkbox" name="source[]" value="News.am"  {{!empty($sourceA) && in_array('News.am',$sourceA)?'checked':''}}>{{__('News.am')}}
            <span class="checkmark"></span>
        </label>
    </div>

    <input type="submit" value="search"/>
</form>