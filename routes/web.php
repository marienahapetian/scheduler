<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return redirect('/news');
});

Route::get('/users',function (){
    $users = \App\User::paginate(10)->onEachSide(5);
    return view('users',compact('users'));
});

Route::get('/news',function (\Illuminate\Http\Request $request){
    $search = '';
    $sourceA = array();

    if($request->input('source') || !empty($request->input('source'))) {
        foreach ($request->input('source') as $key=>$source){
            $sourceA[] = $source;

            if(!isset($articles)) $articles = \App\Article::where('source',$source);
            else $articles = $articles->orWhere('source',$source);
        }
    }

    if( $request->get('search') && $request->get('search')!='' ){
        $search = $request->get('search');

        if(!isset($articles)) $articles = \App\Article::where('title', 'like','%'.$search.'%');
        else $articles = $articles->where('title', 'like','%'.$search.'%');
    }

    if(!isset($articles)) {
        $articles = \App\Article::orderBy('created_at', 'desc')->paginate(10)->onEachSide(5);
    } else {
        $articles = $articles ->orderBy('created_at', 'desc')
            ->paginate(10)->onEachSide(5);
    }

    $articles = $articles->appends(\Illuminate\Support\Facades\Input::except('page'));

    return view('news',compact('articles','search','sourceA'));
});

Route::get('/news/{id}',function ( $id ){
    $article = \App\Article::findOrFail($id);
    return view('single.article',compact('article'));
})->name('article');

Route::get('updatenewsamfeed','ArticleController@updateNewsAmFeed');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/newsfeed', function (){
    $http = new GuzzleHttp\Client;

    $response = $http->get( 'https://newsapi.org/v2/top-headlines?sources=bleacher-report,mtv-news&apiKey=8df92bd7d7e2456bb1f3800c2ed131e7');

    $result = json_decode((string) $response->getBody(), true);

    dd($result['articles']);
});

Route::get('/videos','FileController@index');

Route::get('/video/upload', 'FileController@upload')->middleware('auth');

Route::post('/video/store', 'FileController@store')->middleware('auth');
